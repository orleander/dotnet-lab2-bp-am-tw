﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GameOfLife
{
    class Game
    {
        private Board board;

        public Game(int width, int height)
        {
            board = Board.generateBoard(width, height);
        }

        public void round()
        {
            Board temporary = new Board(board);
            for (int i = 1; i < temporary.getWidth() + 1; i++)
            {
                for (int j = 1; j < temporary.getHeight() + 1; j++)
                {
                    if (temporary.getNeighbourCount(i, j) == 3)
                        board.setCell(i, j, true);

                    if (temporary.getCell(i, j))
                    {
                        if ((temporary.getNeighbourCount(i, j) < 2) || (temporary.getNeighbourCount(i, j) > 3))
                            board.setCell(i, j, false);
                    }
                }
            }
        }

        public void display()
        {
            board.display();
        }
    }
}
