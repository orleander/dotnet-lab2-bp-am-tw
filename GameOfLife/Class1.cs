﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GameOfLife
{
    class Board
    {
        private bool[,] board;

        private int width;
        private int height;

        public static Board generateBoard(int x, int y)
        {
            Board board = new Board(x, y);

            Random rand = new Random();

            for (int i = 0; i < x+1; i++)
                for (int j = 0; j < y+1; j++)
                    board.setCell(i, j, rand.Next(100) < 50);

            for (int i = 0; i < x; i++)
            {
                board.setCell(i, 0, false);
                board.setCell(i, board.height+1, false);
            }

            for (int i = 0; i < y; i++)
            {
                board.setCell(0, i, false);
                board.setCell(board.width+1, i, false);
            }

            return board;
        }

        public Board(int width, int height)
        {
            this.width = width;
            this.height = height;
            board = new bool[width + 2, height + 2];
        }

        public Board(Board original)
        {
            try
            {
                this.width = original.getWidth();
                this.height = original.getHeight();
                this.board = new bool[this.width + 2, this.height + 2];
                for (int i = 0; i < original.getWidth() + 2; i++)
                    for (int j = 0; j < original.getHeight() + 2; j++)
                        board[i, j] = original.getCell(i, j);
            }
            catch (NullReferenceException e)
            {

            }
        }

        public void setCell(int x, int y, bool isAlive)
        {
            try
            {
                board[x, y] = isAlive;
            } catch(IndexOutOfRangeException e)
            {
                e.ToString();
            }
        }

        public bool getCell(int x, int y)
        {
            return board[x, y];
        }

        public int getNeighbourCount(int x, int y)
        {
            int counter = 0;
            for (int i = x - 1; i < x + 2; i++)
            {
                for (int j = y - 1; j < y + 2; j++)
                {
                    if (getCell(i, j))
                    {
                        counter++;
                    }
                }
            }

            if (getCell(x, y)) counter--;

            return counter;
        }

        public int getWidth()
        {
            return width;
        }

        public int getHeight()
        {
            return height;
        }

        public void display()
        {
            for (int i = 1; i < width + 1; i++)
            {
                for (int j = 1; j < height + 1; j++)
                {
                    Console.Write((board[i, j] ? "+" : "O") + " ");
                }

                Console.WriteLine();
            }
        }
    }
}
